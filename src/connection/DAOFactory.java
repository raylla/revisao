package connection;

import java.sql.Connection;

public class DAOFactory {
    
    public static Connection getConexao(){
        return PGConnection.getDatabaseConnection();
    }
}

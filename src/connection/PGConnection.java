package connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class PGConnection {
    
    public static Connection getDatabaseConnection(){
        final String user = "raylla";
        
        try{
            final String driver = "org.postgresql.Driver";
            final String senha = "raylla";
            final String url = "jdbc:postgresql://200.18.128.54/raylla";
            Class.forName(driver);
            
            return DriverManager.getConnection(url,user,senha);
        } catch (ClassNotFoundException ex){
            JOptionPane.showMessageDialog(null, "Erro na conexão! " +ex.getMessage());
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro na conexão! " +ex.getMessage());
        }
        return null;
    }
}

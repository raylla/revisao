package controller;

import controller.helper.CadastrarAlunoHelper;
import model.Aluno;
import model.DAO.AlunoDAO;
import view.CadastrarAluno;

public class CadastrarAlunoController {
    private final CadastrarAluno view;
    private final CadastrarAlunoHelper helper;
    
    public CadastrarAlunoController(CadastrarAluno view){
        this.view = view;
        this.helper = new CadastrarAlunoHelper(view);
    }
    
    public void checkFields(){
        if (view.getTxtCpf().getText().trim().isEmpty() || view.getTxtEmail().getText().trim().isEmpty() ||
                view.getTxtMatricula().getText().trim().isEmpty() || view.getTxtNome().getText().trim().isEmpty() ||
                view.getTxtTelefone().getText().trim().isEmpty()){
            view.exibirMensagem("Preencha todos os campos!");
            
        } else{
            Aluno aluno = helper.getModelo();
            AlunoDAO adao = new AlunoDAO();
            
            adao.insertAluno(aluno);
            helper.limparTela();
        }
    }
}

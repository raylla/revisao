package controller;

import controller.helper.CadastrarProfessorHelper;
import model.DAO.ProfessorDAO;
import model.Professor;
import view.CadastrarProfessor;

public class CadastrarProfessorController {
    private final CadastrarProfessor view;
    private final CadastrarProfessorHelper helper;
    
    public CadastrarProfessorController(CadastrarProfessor view){
        this.view = view;
        this.helper = new CadastrarProfessorHelper(view);
    }
    
    public void checkFields(){
        if(view.getTxtCpf().getText().trim().isEmpty() || view.getTxtEmail().getText().trim().isEmpty() ||
                view.getTxtNome().getText().trim().isEmpty() || view.getTxtTelefone().getText().trim().isEmpty()){
            view.exibirMensagem("Preencha todos os campos!");
            
        } else{
            Professor prof = helper.getModelo();
            ProfessorDAO pdao = new ProfessorDAO();
            
            pdao.insertProfessor(prof);
            helper.limparTela();
        }
    }
}

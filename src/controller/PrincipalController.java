package controller;

import view.CadastrarAluno;
import view.CadastrarProfessor;
import view.Principal;

public class PrincipalController {
    private final Principal view;
    
    public PrincipalController(Principal view){
        this.view = view;
    }
    
    public void navegarSair() {
        System.exit(0);
    }
    
    public void navegarCadastrarAluno(){
        CadastrarAluno cadaluno = new CadastrarAluno();
        view.getjPanel1().add(cadaluno);
        cadaluno.setVisible(true);
    }
    
    public void navegarCadastrarProfessor(){
        CadastrarProfessor cadprof = new CadastrarProfessor();
        view.getjPanel1().add(cadprof);
        cadprof.setVisible(true);
    }
}

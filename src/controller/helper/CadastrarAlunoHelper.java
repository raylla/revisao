package controller.helper;

import model.Aluno;
import view.CadastrarAluno;

public class CadastrarAlunoHelper {
    private final CadastrarAluno view;
    
    public CadastrarAlunoHelper(CadastrarAluno view){
        this.view = view;
    }
    
    public Aluno getModelo(){
        int matricula = Integer.parseInt(view.getTxtMatricula().getText());
        String cpf = view.getTxtCpf().getText();
        String nome = view.getTxtNome().getText();
        String email = view.getTxtEmail().getText();
        String telefone = view.getTxtTelefone().getText();
        
        Aluno modelo = new Aluno (matricula,cpf,nome,email,telefone);
        
        return modelo;
    }
    
    public void limparTela(){
        view.getTxtCpf().setText("");
        view.getTxtEmail().setText("");
        view.getTxtMatricula().setText("");
        view.getTxtNome().setText("");
        view.getTxtTelefone().setText("");
    }
}

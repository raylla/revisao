package controller.helper;

import model.Professor;
import view.CadastrarProfessor;

public class CadastrarProfessorHelper {
    private final CadastrarProfessor view;
    
    public CadastrarProfessorHelper(CadastrarProfessor view){
        this.view = view;
    }
    
    public Professor getModelo(){
        String cpf = view.getTxtCpf().getText();
        String nome = view.getTxtNome().getText();
        String email = view.getTxtEmail().getText();
        String telefone = view.getTxtTelefone().getText();
        
        Professor modelo = new Professor (cpf,nome,email,telefone);
        
        return modelo;
    }
    
    public void limparTela(){
        view.getTxtCpf().setText("");
        view.getTxtEmail().setText("");
        view.getTxtNome().setText("");
        view.getTxtTelefone().setText("");
    }
}

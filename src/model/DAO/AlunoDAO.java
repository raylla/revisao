package model.DAO;

import connection.DAOFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import model.Aluno;

public class AlunoDAO {
    
    public void insertAluno(Aluno aluno){
        
        int res = 0;
        try {
            Connection connection = DAOFactory.getConexao();
            PreparedStatement ps = connection.prepareStatement("INSERT INTO escola.tb_aluno (matricula,cpf,nome,email,"
                    + "telefone) VALUES (?,?,?,?,?)");
            
            ps.setInt(1, aluno.getMatricula());
            ps.setString(2, aluno.getCpf());
            ps.setString(3, aluno.getNome());
            ps.setString(4, aluno.getEmail());
            ps.setString(5, aluno.getTelefone());
            
        res = ps.executeUpdate();
            
        if (res > 0){
        JOptionPane.showMessageDialog(null, "Salvo com sucesso!");
        }
        else{
        JOptionPane.showMessageDialog(null, "Não foi possível salvar!");    
        }
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro na inserção! " +ex.getMessage());
        } 
    }
}

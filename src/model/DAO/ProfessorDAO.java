package model.DAO;

import connection.DAOFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import model.Professor;

public class ProfessorDAO {
    
    public void insertProfessor(Professor professor){
        
        int res = 0;
        try {
            Connection connection = DAOFactory.getConexao();
            PreparedStatement ps = connection.prepareStatement("INSERT INTO escola.tb_professor (cpf,nome,email,"
                    + "telefone) VALUES (?,?,?,?)");
            
            ps.setString(1, professor.getCpf());
            ps.setString(2, professor.getNome());
            ps.setString(3, professor.getEmail());
            ps.setString(4, professor.getTelefone());
            
        res = ps.executeUpdate();
            
        if (res > 0){
        JOptionPane.showMessageDialog(null, "Salvo com sucesso!");
        }
        else{
        JOptionPane.showMessageDialog(null, "Não foi possível salvar!");    
        }
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro na inserção! " +ex.getMessage());
        } 
    }
}
